<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Kriteria</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
				<form method="post" action="<?= base_url($url) ?>" enctype="multipart/form-data">
		            <input type="hidden" value="<?= $detail->id_kriteria ?>" name="id_kriteria" />
					<div class="form-group">
						<label>Bahan</label>
						<input class="form-control" type="text" placeholder="Bahan" name="bahan" value="<?= $detail->bahan ?>" /> 
					</div>
					<div class="form-group">
						<input class="form-control" type="text" placeholder="Bumbu" name="bumbu" value="<?= $detail->bumbu; ?>" /> 
					</div>
					<div class="form-group">
						<label>Jenis Masakan</label>
						<div class="radio">
	                        <label>
	                            <input type="radio" name="jenis_masakan" <?= ($detail->jenis_masakan=='sarapan') ? 'checked' : ''; ?> value="sarapan"> Sarapan
	                        </label>
	                        <label>
	                            <input type="radio" name="jenis_masakan" <?= ($detail->jenis_masakan=='makan_siang') ? 'checked' : ''; ?> value="makan_siang"> Makan Siang
	                        </label>
	                        <label>
	                            <input type="radio" name="jenis_masakan" <?= ($detail->jenis_masakan=='makan_malam') ? 'checked' : ''; ?> value="makan_malam"> Makan Malam
	                        </label>
	                    </div>
					</div>
					<div class="form-group">
						<label>Tingkat Kesulitan</label>
						<div class="radio">
	                        <label>
	                            <input type="radio" name="tingkat_kesulitan" <?= ($detail->tingkat_kesulitan=='pemula') ? 'checked' : ''; ?> value="pemula"> Pemula
	                        </label>
	                        <label>
	                            <input type="radio" name="tingkat_kesulitan" <?= ($detail->tingkat_kesulitan=='menengah') ? 'checked' : ''; ?> value="menengah"> Menengah
	                        </label>
	                        <label>
	                            <input type="radio" name="tingkat_kesulitan" <?= ($detail->tingkat_kesulitan=='ahli') ? 'checked' : ''; ?> value="ahli"> Ahli
	                        </label>
	                    </div>
					</div>
					<div class="form-group">
						<label>Waktu</label>
						<input class="form-control" type="text" placeholder="Waktu" name="waktu" value="<?= $detail->waktu; ?>" /> 
					</div>
					<div class="form-group">
						<label>Daerah Asal</label>
						<input class="form-control" type="text" placeholder="Daerah Asal" name="daerah_asal" value="<?= $detail->daerah_asal; ?>" /> 
					</div>
					<div class="form-group">
						<label>Bobot</label>
						<input class="form-control" type="text" placeholder="Bobot" name="bobot" value="<?= $detail->bobot; ?>" /> 
					</div>
					<div class="form-group">
						<a href="<?= base_url($kembali) ?>" class="btn btn-default">Kembali</a>
						<input type="submit" class="btn btn-primary" value="Update" />
					</div>
				</form>
			</div>
		</div>
	</div>
</div>