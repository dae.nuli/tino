<div id="page-wrapper">

    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Kriteria</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
				<form method="post" enctype="multipart/form-data" action="<?= base_url($url) ?>">
					<div class="form-group">
						<label>Bahan</label>
						<input class="form-control" type="text" placeholder="Bahan" name="bahan" /> 
					</div>
					<div class="form-group">
						<label>Bumbu</label>
						<input class="form-control" type="text" placeholder="Bumbu" name="bumbu" /> 
					</div>
					<div class="form-group">
						<label>Jenis Masakan</label>
						<div class="radio">
	                        <label>
	                            <input type="radio" name="jenis_masakan" value="sarapan"> Sarapan
	                        </label>
	                        <label>
	                            <input type="radio" name="jenis_masakan" value="makan_siang"> Makan Siang
	                        </label>
	                        <label>
	                            <input type="radio" name="jenis_masakan" value="makan_malam"> Makan Malam
	                        </label>
	                    </div>
					</div>
					<div class="form-group">
						<label>Tingkat Kesulitan</label>
						<div class="radio">
	                        <label>
	                            <input type="radio" name="tingkat_kesulitan" value="pemula"> Pemula
	                        </label>
	                        <label>
	                            <input type="radio" name="tingkat_kesulitan" value="menengah"> Menengah
	                        </label>
	                        <label>
	                            <input type="radio" name="tingkat_kesulitan" value="ahli"> Ahli
	                        </label>
	                    </div>
					</div>
					<div class="form-group">
						<label>Waktu</label>
						<input class="form-control" type="text" placeholder="Waktu" name="waktu" /> 
					</div>
					<div class="form-group">
						<label>Daerah Asal</label>
						<input class="form-control" type="text" placeholder="Daerah Asal" name="daerah_asal" /> 
					</div>
					<div class="form-group">
						<label>Bobot</label>
						<input class="form-control" type="text" placeholder="Bobot" name="bobot" /> 
					</div>
					<div class="form-group">
						<a href="<?= base_url($kembali) ?>" class="btn btn-default">Kembali</a>
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
