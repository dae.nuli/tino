<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Kriteria</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
            	<a href="<?= base_url($url.'create') ?>" class="btn btn-default">Tambah Kriteria</a><br><br>
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
								<th>No. </th>
								<th>Bahan</th>
								<th>Bumbu</th>
                                <th>Jenis Masakan</th>
                                <th>Tingkat Kesulitan</th>
                                <th>Waktu</th>
                                <th>Daerah Asal</th>
								<th>Bobot</th>
								<th></th>
                            </tr>
                        </thead>
                        <tbody>
    					<?php
							$i = 1;
							foreach ($index->result() as $row) {
						?>
						<tr>
							<td><?= $i++ ?></td>
							<td><?= $row->bahan ?></td>
                            <td><?= $row->bumbu ?></td>
                            <td><?= $row->jenis_masakan ?></td>
                            <td><?= $row->tingkat_kesulitan ?></td>
                            <td><?= $row->waktu ?></td>
                            <td><?= $row->daerah_asal ?></td>
							<td><?= $row->bobot ?></td>
							<td>
								<a href="<?= base_url($url.'update/'.$row->id_kriteria) ?>" class="btn btn-xs btn-info">Edit</a>
								<a href="<?= base_url($url.'delete/'.$row->id_kriteria) ?>" class="btn btn-xs btn-danger">Delete</a>
							</td>
						</tr>
						<?php
							}
						?>
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
</div>