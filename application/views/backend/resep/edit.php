<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Resep
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
				<form method="post" action="<?= base_url($url) ?>" enctype="multipart/form-data">
		            <input type="hidden" value="<?= $detail->id_resep ?>" name="id_resep" />
					<div class="form-group">
						<label>Nama Resep</label>
						<input class="form-control" type="text" placeholder="Nama Resep" name="nama_resep" value="<?= $detail->nama_resep ?>" /> 
					</div>
					<div class="form-group">
						<label>Detail Resep</label>
						<input class="form-control" type="text" placeholder="Detail Resep" name="detail_resep" value="<?= $detail->detail_resep; ?>" /> 
					</div>
					<div class="form-group">
						<label>Kriteria</label>
                        <select class="form-control" name="kriteria">
                        	<option>- Pilih Kriteria -</option>
                        	<?php
								foreach ($kriteria->result() as $row) {
									if($row->id_kriteria==$detail->id_kriteria){
										echo "<option value='".$row->id_kriteria."' selected>".$row->bahan."</option>";	
									}else{
										echo "<option value='".$row->id_kriteria."'>".$row->bahan."</option>";	
									}
								}
                        	?>
                        </select>
					</div>
					<div class="form-group">
						<label>Gambar</label>
						<input type="file" name="gambar_resep" /> 
					</div>
					<div class="form-group">
						<a href="<?= base_url($kembali) ?>" class="btn btn-default">Kembali</a>
						<input type="submit" class="btn btn-primary" value="Update" />
					</div>
				</form>
			</div>
		</div>
	</div>
</div>