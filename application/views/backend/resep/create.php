<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Resep
                </h1>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">

            <div class="col-lg-6">
				<form method="post" enctype="multipart/form-data" action="<?= base_url($url) ?>">
					<div class="form-group">
						<label>Nama Resep</label>
						<input class="form-control" type="text" placeholder="Nama Resep" name="nama_resep" />
					</div>
					<div class="form-group">
						<label>Detail Resep</label>
						<input class="form-control" type="text" placeholder="Detail Resep" name="detail_resep" />
					</div>
					<div class="form-group">
						<label>Kriteria</label>
                        <select class="form-control" name="kriteria">
                        	<option>- Pilih Kriteria -</option>
                        	<?php
								foreach ($kriteria->result() as $row) {
									echo "<option value='".$row->id_kriteria."'>".$row->bahan."</option>";	
								}
                        	?>
                        </select>
					</div>
					<div class="form-group">
						<label>Gambar</label>
						<input type="file" name="gambar_resep" />
					</div>
					<div class="form-group">
						<a href="<?= base_url($kembali) ?>" class="btn btn-default">Kembali</a>
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
