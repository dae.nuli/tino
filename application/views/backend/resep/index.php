<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Resep
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
            	<a href="<?= base_url($url.'create') ?>" class="btn btn-default">Tambah Resep</a><br><br>
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
								<th>No. </th>
								<th>Nama Resep</th>
								<th>Detail Resep</th>
								<th>Gambar Resep</th>
								<th></th>
                            </tr>
                        </thead>
                        <tbody>
    					<?php
							$i = 1;
							foreach ($index->result() as $row) {
						?>
						<tr>
							<td><?= $i++ ?></td>
							<td><?= $row->nama_resep ?></td>
							<td><?= $row->detail_resep ?></td>
							<td><img width="100" src="<?= base_url() ?>/assets/upload/<?= $row->gambar_resep ?>"/></td>
							<td>
								<a href="<?= base_url($url.'update/'.$row->id_resep) ?>" class="btn btn-xs btn-info">Edit</a>
								<a href="<?= base_url($url.'delete/'.$row->id_resep) ?>" class="btn btn-xs btn-danger">Delete</a>
							</td>
						</tr>
						<?php
							}
						?>
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
</div>