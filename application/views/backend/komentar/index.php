<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Komentar
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
								<th>No. </th>
								<th>Nama</th>
                                <th>Email</th>
                                <th>No. Handphone</th>
                                <th>Komentar</th>
                                <th>Resep</th>
								<th>Tanggal</th>
								<th></th>
                            </tr>
                        </thead>
                        <tbody>
    					<?php
							$i = 1;
							foreach ($index->result() as $row) {
						?>
						<tr>
							<td><?= $i++ ?></td>
							<td><?= $row->nama ?></td>
                            <td><?= $row->email ?></td>
                            <td><?= $row->handphone ?></td>
                            <td><?= $row->isi ?></td>
                            <td><?= $row->id_resep ?></td>
							<td><?= date('d F Y',strtotime($row->tanggal)) ?></td>
							<td>
								<a href="<?= base_url($url.'update/'.$row->id_admin) ?>" class="btn btn-xs btn-info">Edit</a>
								<a href="<?= base_url($url.'delete/'.$row->id_admin) ?>" class="btn btn-xs btn-danger">Delete</a>
							</td>
						</tr>
						<?php
							}
						?>
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
</div>