<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Admin
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
				<form method="post" action="<?= base_url($url) ?>" enctype="multipart/form-data">
		            <input type="hidden" value="<?= $detail->id_admin ?>" name="id_admin" />
					<div class="form-group">
						<label>Nama</label>
						<input class="form-control" type="text" placeholder="Nama" name="nama" value="<?= $detail->nama ?>" /> 
					</div>
					<div class="form-group">
						<label>Email</label>
						<input class="form-control" type="email" placeholder="Email" name="email" value="<?= $detail->email; ?>" /> 
					</div>
					<div class="form-group">
						<label>Password</label>
						<input class="form-control" type="password" placeholder="Password" name="password"/> 
					</div>
					<div class="form-group">
						<a href="<?= base_url($kembali) ?>" class="btn btn-default">Kembali</a>
						<input type="submit" class="btn btn-primary" value="Update" />
					</div>
				</form>
			</div>
		</div>
	</div>
</div>