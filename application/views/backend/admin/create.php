<div id="page-wrapper">
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Admin
                </h1>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-6">
				<form method="post" enctype="multipart/form-data" action="<?= base_url($url) ?>">
					<div class="form-group">
						<label>Nama</label>
						<input class="form-control" type="text" placeholder="Nama" name="nama" />
					</div>
					<div class="form-group">
						<label>Email</label>
						<input class="form-control" type="email" placeholder="Email" name="email" />
					</div>
					<div class="form-group">
						<label>Password</label>
						<input class="form-control" type="password" placeholder="Password" name="password" />
					</div>
					<div class="form-group">
						<a href="<?= base_url($kembali) ?>" class="btn btn-default">Kembali</a>
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>