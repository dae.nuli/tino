<!-- File update_product_view.php -->
<html>
	<head>
		<title>Edit Resep</title>
	</head>
	<body>
		<h1>Edit Resep</h1>

		<form method="post" action="<?= base_url($url) ?>" enctype="multipart/form-data">
            <input type="hidden" value="<?= $detail->id_resep ?>" name="id_resep" />
			<input type="text" placeholder="Nama Resep" name="nama_resep" value="<?= $detail->nama_resep ?>" /> 
			<input type="text" placeholder="Detail Resep" name="detail_resep" value="<?= $detail->detail_resep; ?>" /> 
			<input type="file" name="gambar_resep" /> 
			<input type="submit" value="Update" />
		</form>

	</body>
</html>