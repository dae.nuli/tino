<html>
	<head>
		<title>Tambah Resep</title>
	</head>
	<body>
		<h1>Tambah Resep</h1>
		<form method="post" enctype="multipart/form-data" action="<?= base_url($url) ?>">
			<!-- action merupakan halaman yang dituju ketika tombol submit dalam suatu form ditekan -->
			<input type="text" placeholder="Nama Resep" name="nama_resep" />
			<input type="text" placeholder="Detail Resep" name="detail_resep" />
			<input type="file" name="gambar_resep" />
			<input type="submit" />
		</form>
	</body>
</html>