<html>
	<head>
		<title>CRUD dengan CodeIgniter</title>
	</head>
	<body>

			<!-- Kalau datanya masih kosong, kita harus melakukan add product -->
			<a href="<?= base_url($url.'/create') ?>">Tambah Produk</a>
<?php echo ENVIRONMENT; ?>
			<!-- Kalau ada datanya, maka kita akan tampilkan dalam table -->
			<h1>Products List</h1>
			<table border="1">
				<thead>
					<tr>
						<th>No. </th>
						<th>Nama Resep</th>
						<th>Detail Resep</th>
						<th>Gambar Resep</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php
						//Kita akan melakukan looping sesuai dengan data yang dimiliki
						$i = 1; //nantinya akan digunakan untuk pengisian Nomor
						foreach ($index->result() as $row) {
					?>
					<tr>
						<td><?= $i++ ?></td>
						<td><?= $row->nama_resep ?></td>
						<td><?= $row->detail_resep ?></td>
						<td><img width="100" src="assets/upload/<?= $row->gambar_resep ?>"/></td>
						<td>
							<!-- Akan melakukan update atau delete sesuai dengan id yang diberikan ke controller -->
							<a href="<?= base_url($url.'update/'.$row->id_resep) ?>">Update</a>
							|
							<a href="<?= base_url($url.'delete/'.$row->id_resep) ?>">Delete</a>
						</td>
					</tr>
					<?php
						}
					?>
				</tbody>
			</table>

	</body>
</html>