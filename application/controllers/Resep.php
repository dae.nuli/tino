<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Resep extends CI_Controller  {
	private $folder;
	private $url;
	function __construct(){
		parent::__construct();
		$this->load->model("resep_model"); //constructor yang dipanggil ketika memanggil products.php untuk melakukan pemanggilan pada model : products_model.php yang ada di folder models
		$this->folder = 'back/resep/';
		$this->url    = 'resep/';
	}

	public function index()
	{
		$data['index'] = $this->resep_model->getAllResep();
		$data['url']   = $this->url;
		$this->load->view($this->folder.'index', $data); 
	}

	public function create()
	{
		$data['url']   = $this->url.'addResepDb';
		$this->load->view($this->folder.'create', $data);
	}

	public function addResepDb()
	{
		$new_name = rand(111111,999999);
		$config['upload_path']   = 'assets/upload'; 
		$config['allowed_types'] = 'gif|jpg|png';
		$config['file_name']     = $new_name;
        // $config['max_size']      = 100; 
        // $config['max_width']     = 1024; 
        // $config['max_height']    = 768;  
        $this->load->library('upload', $config);
			
        if ( ! $this->upload->do_upload('gambar_resep')) {
            echo $this->upload->display_errors();
        }else { 
            $photo = $this->upload->data();
            $data = array(
				'nama_resep'   => $this->input->post('nama_resep'),
				'detail_resep' => $this->input->post('detail_resep'),
				'gambar_resep' => $new_name.$photo['file_ext']
			);
			$this->resep_model->addResep($data); //passing variable $data ke products_model

			redirect($this->url);
        } 
		
	}

	public function update($idResep)
	{
		$data['url'] = $this->url.'updateResepDb';
		$data['detail'] = $this->resep_model->getResep($idResep)->row(); //Melakukan pemanggilan fungsi getProduct yang ada di dalam products_model untuk mendapatkan informasi / data mengenai produk berdasarkan productId yang dikirimkan
        
        $this->load->view($this->folder.'edit', $data);
	}

	public function updateResepDb()
	{

		$new_name = rand(111111,999999);
		$config['upload_path']   = 'assets/upload'; 
		$config['allowed_types'] = 'gif|jpg|png';
		$config['file_name']     = $new_name;
        // $config['max_size']      = 100; 
        // $config['max_width']     = 1024; 
        // $config['max_height']    = 768;  
        $this->load->library('upload', $config);
			
        if ( ! $this->upload->do_upload('gambar_resep')) {
            echo $this->upload->display_errors();
        }else { 
            $photo = $this->upload->data();
        }
        if(!empty($photo)){
	        $data = array(
				'nama_resep'   => $this->input->post('nama_resep'),
				'detail_resep' => $this->input->post('detail_resep'),
				'gambar_resep' => $new_name.$photo['file_ext']
			);
        }else{
	        $data = array(
				'nama_resep'   => $this->input->post('nama_resep'),
				'detail_resep' => $this->input->post('detail_resep')
			);
        }

        $condition['id_resep'] = $this->input->post('id_resep'); //Digunakan untuk melakukan validasi terhadap produk mana yang akan diupdate nantinya
        
		$this->resep_model->updateResep($data, $condition); //passing variable $data ke products_model

		redirect($this->url);
	}

	public function delete($id)
	{
		$this->resep_model->deleteResep($id); //Memanggil fungsi deleteProduct yang ada pada model products_model dan mengirimkan parameter yaitu productId yang akan di delete
		redirect($this->url);
	}
}