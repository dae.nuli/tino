<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pengguna extends CI_Controller  {
	private $folder;
	private $url;
	function __construct(){
		parent::__construct();
		$this->load->model("admin_model"); 
		$this->folder  = 'backend/admin/';
		$this->url     = 'admin/pengguna/';
		$this->backend = 'backend/';
		$this->kembali = 'admin/pengguna';
	}

	public function index()
	{
		$data['index'] = $this->admin_model->getAllAdmin();
		$data['url']   = $this->url;
		$this->load->view($this->backend.'header'); 
		$this->load->view($this->folder.'index', $data); 
		$this->load->view($this->backend.'footer'); 
	}

	public function create()
	{
		$data['url']     = $this->url.'addAdminDb';
		$data['kembali'] = $this->kembali;
		$this->load->view($this->backend.'header'); 
		$this->load->view($this->folder.'create', $data);
		$this->load->view($this->backend.'footer'); 
	}

	public function addAdminDb()
	{
        $data = array(
			'nama'     => $this->input->post('nama'),
			'email'    => $this->input->post('email'),
			'password' => md5($this->input->post('password'))
		);
		$this->admin_model->addAdmin($data);
		redirect($this->url);
	}

	public function update($idAdmin)
	{
		$data['url']     = $this->url.'updateAdminDb';
		$data['detail']  = $this->admin_model->getAdmin($idAdmin)->row();
		
		$data['kembali'] = $this->kembali;
		$this->load->view($this->backend.'header'); 
        $this->load->view($this->folder.'edit', $data);
		$this->load->view($this->backend.'footer'); 
	}

	public function updateAdminDb()
	{
		if(!empty($this->input->post('password'))){
			$data['password'] = md5($this->input->post('password'));
		}
        $data = array(
			'nama'     => $this->input->post('nama'),
			'email'    => $this->input->post('email')
		);
        $condition['id_admin'] = $this->input->post('id_admin'); 
		$this->admin_model->updateAdmin($data, $condition); 
		redirect($this->url);
	}

	public function delete($id)
	{
		$this->admin_model->deleteAdmin($id);
		redirect($this->url);
	}
}