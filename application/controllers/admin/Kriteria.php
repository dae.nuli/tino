<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Kriteria extends CI_Controller  {
	private $folder;
	private $url;
	function __construct(){
		parent::__construct();
		$this->load->model("kriteria_model"); 
		$this->folder  = 'backend/kriteria/';
		$this->url     = 'admin/kriteria/';
		$this->backend = 'backend/';
		$this->kembali = 'admin/kriteria';
	}

	public function index()
	{
		$data['index'] = $this->kriteria_model->getAllKriteria();
		$data['url']   = $this->url;
		$this->load->view($this->backend.'header'); 
		$this->load->view($this->folder.'index', $data); 
		$this->load->view($this->backend.'footer'); 
	}

	public function create()
	{
		$data['url']     = $this->url.'addKriteriaDb';
		$data['kembali'] = $this->kembali;
		$this->load->view($this->backend.'header'); 
		$this->load->view($this->folder.'create', $data);
		$this->load->view($this->backend.'footer'); 
	}

	public function addKriteriaDb()
	{
        $data = array(
			'bahan'             => $this->input->post('bahan'),
			'bumbu'             => $this->input->post('bumbu'),
			'jenis_masakan'     => $this->input->post('jenis_masakan'),
			'tingkat_kesulitan' => $this->input->post('tingkat_kesulitan'),
			'waktu'             => $this->input->post('waktu'),
			'daerah_asal'       => $this->input->post('daerah_asal'),
			'bobot'             => $this->input->post('bobot')
		);
		$this->kriteria_model->addKriteria($data);

		redirect($this->url);
	}

	public function update($idKriteria)
	{
		$data['url']     = $this->url.'updateKriteriaDb';
		$data['detail']  = $this->kriteria_model->getKriteria($idKriteria)->row();
		$data['kembali'] = $this->kembali;

		$this->load->view($this->backend.'header'); 
        $this->load->view($this->folder.'edit', $data);
		$this->load->view($this->backend.'footer'); 
	}

	public function updateKriteriaDb()
	{
        $data = array(
			'bahan'             => $this->input->post('bahan'),
			'bumbu'             => $this->input->post('bumbu'),
			'jenis_masakan'     => $this->input->post('jenis_masakan'),
			'tingkat_kesulitan' => $this->input->post('tingkat_kesulitan'),
			'waktu'             => $this->input->post('waktu'),
			'daerah_asal'       => $this->input->post('daerah_asal'),
			'bobot'             => $this->input->post('bobot')
		);
        $condition['id_kriteria'] = $this->input->post('id_kriteria'); 
		$this->kriteria_model->updateKriteria($data, $condition); 
		redirect($this->url);
	}

	public function delete($id)
	{
		$this->kriteria_model->deleteKriteria($id);
		redirect($this->url);
	}
}