<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Resep extends CI_Controller  {
	private $folder;
	private $url;
	function __construct(){
		parent::__construct();
		$this->load->model("resep_model"); 
		$this->load->model("kriteria_model");
		$this->folder  = 'backend/resep/';
		$this->url     = 'admin/resep/';
		$this->backend = 'backend/';
		$this->kembali = 'admin/resep';
	}

	public function index()
	{
		$data['index']   = $this->resep_model->getAllResep();
		$data['url']     = $this->url;
		$this->load->view($this->backend.'header'); 
		$this->load->view($this->folder.'index', $data); 
		$this->load->view($this->backend.'footer'); 
	}

	public function create()
	{
		$data['url']      = $this->url.'addResepDb';
		$data['kriteria'] = $this->kriteria_model->getAllKriteria();
		$data['kembali']  = $this->kembali;
		$this->load->view($this->backend.'header'); 
		$this->load->view($this->folder.'create', $data);
		$this->load->view($this->backend.'footer'); 
	}

	public function addResepDb()
	{
		$new_name = rand(111111,999999);
		$config['upload_path']   = 'assets/upload'; 
		$config['allowed_types'] = 'gif|jpg|png';
		$config['file_name']     = $new_name;
        // $config['max_size']      = 100; 
        // $config['max_width']     = 1024; 
        // $config['max_height']    = 768;  
        $this->load->library('upload', $config);
			
        if ( ! $this->upload->do_upload('gambar_resep')) {
            echo $this->upload->display_errors();
        }else { 
			$photo = $this->upload->data();
			$data  = array(
				'nama_resep'   => $this->input->post('nama_resep'),
				'detail_resep' => $this->input->post('detail_resep'),
				'id_kriteria'  => $this->input->post('kriteria'),
				'gambar_resep' => $new_name.$photo['file_ext']
			);
			$this->resep_model->addResep($data);

			redirect($this->url);
        } 
		
	}

	public function update($idResep)
	{
		$data['url']      = $this->url.'updateResepDb';
		$data['detail']   = $this->resep_model->getResep($idResep)->row();
		$data['kembali']  = $this->kembali;
		$data['kriteria'] = $this->kriteria_model->getAllKriteria();
		$this->load->view($this->backend.'header'); 
        $this->load->view($this->folder.'edit', $data);
		$this->load->view($this->backend.'footer'); 
	}

	public function updateResepDb()
	{

		$new_name = rand(111111,999999);
		$config['upload_path']   = 'assets/upload'; 
		$config['allowed_types'] = 'gif|jpg|png';
		$config['file_name']     = $new_name;
        // $config['max_size']      = 100; 
        // $config['max_width']     = 1024; 
        // $config['max_height']    = 768;  
        $this->load->library('upload', $config);
			
        if ( ! $this->upload->do_upload('gambar_resep')) {
            echo $this->upload->display_errors();
        }else { 
            $photo = $this->upload->data();
        }
        if(!empty($photo)){
	        $data = array(
				'nama_resep'   => $this->input->post('nama_resep'),
				'detail_resep' => $this->input->post('detail_resep'),
				'id_kriteria'  => $this->input->post('kriteria'),
				'gambar_resep' => $new_name.$photo['file_ext']
			);
        }else{
	        $data = array(
				'nama_resep'   => $this->input->post('nama_resep'),
				'id_kriteria'  => $this->input->post('kriteria'),
				'detail_resep' => $this->input->post('detail_resep')
			);
        }

        $condition['id_resep'] = $this->input->post('id_resep');
		$this->resep_model->updateResep($data, $condition);
		redirect($this->url);
	}

	public function delete($id)
	{
		$this->resep_model->deleteResep($id);
		redirect($this->url);
	}
}