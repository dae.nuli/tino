<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Komentar extends CI_Controller  {
	private $folder;
	private $url;
	function __construct(){
		parent::__construct();
		$this->load->model("komentar_model"); 
		$this->folder  = 'backend/komentar/';
		$this->url     = 'admin/komentar/';
		$this->backend = 'backend/';
	}

	public function index()
	{
		$data['index'] = $this->komentar_model->getAllKomentar();
		$data['url']   = $this->url;
		$this->load->view($this->backend.'header'); 
		$this->load->view($this->folder.'index', $data); 
		$this->load->view($this->backend.'footer'); 
	}

	public function delete($id)
	{
		$this->komentar_model->deleteKomentar($id);
		redirect($this->url);
	}
}