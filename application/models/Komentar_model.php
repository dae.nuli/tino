<?php
	class Komentar_model extends CI_Model  {
	private $table;
	function __construct() { 
		parent::__construct(); 
		$this->table = 'komentar';
	}
	
	function getAllKomentar() {
		$this->db->from($this->table);
		return $this->db->get();
	}

	function deleteKomentar($id)
	{
        $this->db->where('id_komentar', $id);
        $this->db->delete($this->table);
	}
}