<?php
	class Resep_model extends CI_Model  {
	private $table;
	function __construct() { 
		parent::__construct(); 
		$this->table = 'resep_masakan';
	}
	
	function getAllResep() {
		$this->db->from($this->table);
		return $this->db->get();
	}

	function getResep($id)
	{
        $this->db->where('id_resep', $id); 
        $this->db->select("*");
        $this->db->from($this->table);
        
        return $this->db->get();
	}

	function addResep($data)
	{
		$this->db->insert($this->table, $data);
	}

	function updateResep($data, $condition)
	{
		$this->db->where($condition);
		$this->db->update($this->table, $data);
	}

	function deleteResep($id)
	{
        $this->db->where('id_resep', $id);
        $this->db->delete($this->table);
	}
}