<?php
	class Admin_model extends CI_Model  {
	private $table;
	function __construct() { 
		parent::__construct(); 
		$this->table = 'admin';
	}
	
	function getAllAdmin() {
		$this->db->from($this->table);
		return $this->db->get();
	}

	function getAdmin($id)
	{
        $this->db->where('id_admin', $id); 
        $this->db->select("*");
        $this->db->from($this->table);
        
        return $this->db->get();
	}

	function addAdmin($data)
	{
		$this->db->insert($this->table, $data);
	}

	function updateAdmin($data, $condition)
	{
		$this->db->where($condition);
		$this->db->update($this->table, $data);
	}

	function deleteAdmin($id)
	{
        $this->db->where('id_admin', $id);
        $this->db->delete($this->table);
	}
}