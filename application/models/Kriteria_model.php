<?php
	class Kriteria_model extends CI_Model  {
	private $table;
	function __construct() { 
		parent::__construct(); 
		$this->table = 'kriteria_masakan';
	}
	
	function getAllKriteria() {
		$this->db->from($this->table);
		return $this->db->get();
	}

	function getKriteria($id)
	{
        $this->db->where('id_kriteria', $id); 
        $this->db->select("*");
        $this->db->from($this->table);
        
        return $this->db->get();
	}

	function addKriteria($data)
	{
		$this->db->insert($this->table, $data);
	}

	function updateKriteria($data, $condition)
	{
		$this->db->where($condition);
		$this->db->update($this->table, $data);
	}

	function deleteKriteria($id)
	{
        $this->db->where('id_kriteria', $id);
        $this->db->delete($this->table);
	}
}